package juego;


import java.awt.Color;
import java.awt.Font;

import java.awt.Image;
import java.util.Arrays;
import java.util.Random;


import javax.sound.sampled.Clip;
import TablaRank.Jugada;
import TablaRank.Ranking;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;


public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
	//###########################  PRUEBAS PARA EL RANKING Y DIFICULTAD
	private String playerName;
	private boolean seGuardo;
	private boolean esNormal;
	private boolean hayBalas;
	private boolean	hayMusica;
	private double	velocidad;
	private boolean enPausa;
	private Clip musiquita;
	// Variables y metodos propios de cada grupo
	
	//###########################  IMAGENES DEL JUEGO
	private Image pause;
	private Image soldadoDead;
	private Image imagenFondo;
	private Image fueguillos;
	private Image Corazon;
	private Image  suelo;
	private Image gameOver;
	private Image princesaSalto;
	private Image Arbol1;
	private Image Arbol2;
	private Image Arbol3;
	private Image Arbol4;
	private Image Arbol5;
	private Image [] packFondoArboles;
	private Bala	balas;
	private Obstaculo [] obstaculos;
	private Soldado	[] soldados;
	private Fireball [] fireballs;
	private Princesa Princesa;
	private Vida vidas;
	
	private int score;
	private int vidasTotal;
	private int vidasMaximas;
	private boolean hayItem;
	private MovimientoFondo arboles;
	Random rand = new Random();
	private int unaBola;
	private int maxFire;
	private int cantiObstaculos;
	private int cantiSoldados;
	
	private double ticks;
	
	
	//Variables Necesarios Para el salto de la princesa y soldado 
	private boolean princesaSube;
	private boolean princesaBaja;
	private boolean [] soldadoSube;
	private boolean [] soldadoBaja;
	private boolean noMeVeas;
	private boolean siChoco;
	private int indice;
	private int time;
	private boolean esInmune;
	
	//
	


	
	//###########################################################################
	//					FUNCIONES PARA DETERMINAR COLISION 					   //
	//###########################################################################
	
	
	public boolean chocaVida() {
		if ( vidas.x1() <=  Princesa.x2() && vidas.x1() >=  Princesa.x1() && vidas.getY() <= Princesa.y2() && vidas.getY() >= Princesa.y1() ){
			return true;
		}
		if(vidas.x2() >= Princesa.x1() && vidas.x2() <= Princesa.x2() && vidas.getY() <= Princesa.y2() && vidas.getY() >= Princesa.y1() ) {
			return true;
		}
		return false;
	}

	
	//###########################################################################
	//					FUNCIONES PARA LA CREACION DE OBJETOS	   		  	   //
	//###########################################################################
	
	
	// Determina la altura del Piso 
	double damePiso() {
		double piso = (this.entorno.alto() - this.entorno.alto() / 6) - 30;
		return piso;
	}
	
	public Obstaculo dameObsta(double separacion) {
		Obstaculo  bulto = new Obstaculo(800 +separacion, damePiso() + 20 , 36 , 65, velocidad);
		return bulto;
	}
	
	
	public Bala dameBalas() {
		Bala  bala = new Bala(795, damePiso() - rand.nextInt(40)-110 , 35 , 20, velocidad );
		return bala;
	}
	
	

	public Soldado dameSoldados(double separador) {
		Soldado normal= new Soldado(800 + separador, damePiso() , 50 , 70 ,velocidad);
		return normal;
	}
	
	public Soldado [] dameSoldaList(int num) {
		Soldado [] peloton= new Soldado[num];
		double separador = 0;
		for (int i = 0; i < peloton.length; i++) {
			peloton[i] = dameSoldados(separador);
			separador = separador + (rand.nextInt(193) + 160);
		}
		return peloton;
	}
	
	public boolean todosMuertos(Soldado [] lista) {
		for (Soldado soldado : lista) {
			if(soldado !=null) {
				return false;
			}
		}
		return true;
	}
	
	public void reseteaLista(boolean [] lista) {
		Arrays.fill(lista, false);
	}
	
	
	public Obstaculo [] dameObstaList(int num) {
		Obstaculo [] molestias= new Obstaculo[num];
		double separador = 0;
		for (int i = 0; i < molestias.length; i++) {
			molestias[i] = dameObsta(separador);
			separador = separador + (rand.nextInt(333) + 200);
		}
		return molestias;
	}
	
	
	public Fireball [] fireListNull(int cantidad) {
		Fireball [] fuegos = new Fireball[cantidad];
		Arrays.fill(fuegos, null);
		return fuegos;
	}
	
	
	public Vida dameCorazon() {
		
		Vida corazoncito = new Vida(800, damePiso() - 100, 40 , 30,velocidad);
		return corazoncito;
	}
	
	public MovimientoFondo dameArboles () {
		
	 	MovimientoFondo unArbol = new MovimientoFondo(dameImagenArbol(),800+rand.nextInt(1000),damePiso()-240,velocidad); 
		return unArbol;
	}

	public Image dameImagenArbol() {
			return this.packFondoArboles[rand.nextInt(this.packFondoArboles.length)];
	}
	


	//###########################################################################
	//	 INICIO FUNCION JUEGO Y VARIABLES NECESARIAS PARA EL FUNCIONAMIENTO	   //
	//###########################################################################
	
	Juego(String nombreJugador,boolean dificultad,boolean musica)
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 8 - v1", 800, 600);
		
		musiquita = Herramientas.cargarSonido("gravity-falls.wav");
		
		//#######################################################################
		//		SE DETERMINA LA DIFICULTAD  Y SE INICIALIZA EN BASE A ELLO 	   //
		//#######################################################################
		
		esNormal = dificultad;
		hayMusica = musica;
		if(esNormal) {
			velocidad = 0;
			cantiObstaculos = 4;		// CANTIDAD DE OBSTACULOS
			cantiSoldados = 3;			// CANTIDAD DE SOLDADOS
			hayBalas = false;
		}
		if(!esNormal) {
			velocidad = 1;
			cantiObstaculos = 5;
			cantiSoldados = 5;			// CANTIDAD DE SOLDADOS
			hayBalas = true;
		}
		if(hayMusica) {
			musiquita.loop(Clip.LOOP_CONTINUOUSLY);
		}
		
		
		//#######################################################
		//		INICIA EL CARGADO DE TODAS LAS IMAGENES	   	   //
		//#######################################################
		// Inicializar lo que haga falta para el juego
		this.imagenFondo = Herramientas.cargarImagen("fondo.png");
		this.Corazon= Herramientas.cargarImagen("corazon.png");
		this.suelo = Herramientas.cargarImagen("camino.png");
		this.gameOver = Herramientas.cargarImagen("gameOver.gif");
		this.princesaSalto = Herramientas.cargarImagen("princesa.png");
		this.soldadoDead = Herramientas.cargarImagen("soldado76muerto.png");
		this.Arbol1 = Herramientas.cargarImagen("arbol1.png");
		this.Arbol2 = Herramientas.cargarImagen("arbol2.png");
		this.Arbol3 = Herramientas.cargarImagen("arbol3.png");
		this.Arbol4 = Herramientas.cargarImagen("arbol4.png");
		this.Arbol5 = Herramientas.cargarImagen("arbol5.png");
		this.packFondoArboles = new Image [] {this.Arbol1, this.Arbol2, this.Arbol3,this.Arbol4, this.Arbol5};
		this.arboles = dameArboles ();
		this.pause	= Herramientas.cargarImagen("pausa.gif");
		this.fueguillos = Herramientas.cargarImagen("Fireball1.png");
		
		
		//#######################################################
		//		INICIA PRINCESA Y SUS VARIABLES DE CONTROL     //
		//#######################################################
		this.Princesa   = new Princesa(200, damePiso() , 60  ,this.entorno.alto()/5.45); 
		princesaSube = false;
		princesaBaja = false;    //PARA SABER SI PRINCESA ESTA SUBIENDO EN EL SALTO O ESTA BAJANDO
		esInmune = false;		// PARA CONTROLAR LA INMUNIDAD LUEGO DE UNA COLISION
		noMeVeas = true;
		
		// SE ASIGNA VALORES A LAS VARIABLE DE OBSTACULOS FIREBALL 

		if (hayBalas) {					//DEPENDE  DE LA DIFICULTAD  SI HAY BALAS O NO EN EL JUEGO ACTUAL 
			this.balas = dameBalas();
		}
		maxFire = 3;
		unaBola = 0;
		fireballs = fireListNull(maxFire);
		time = 3;					// TIEMPO DE INMUNIDAD DESPUES DE SUFRIR DAÑO
		
		hayItem = false;
		this.vidas = dameCorazon();			// CREA ITEMS VIDAS
		
		//#######################################################
		//			VARIABLES DE CONTROL DE SOLDADO			   //
		//#######################################################
		
		soldados = dameSoldaList(cantiSoldados);
		
		soldadoSube = new boolean[cantiSoldados];			// SON PARA EL CONTROL DEL SALTO DEL SOLDADO
		reseteaLista(soldadoSube);
		soldadoBaja = new boolean[cantiSoldados];
		reseteaLista(soldadoBaja);
		
		//#######################################################
		//			VARIABLES DE CONTROL DE COLISIONES		   //
		//#######################################################
		siChoco = false;		// SE USA DE FLAG PARA CONTROLAR ALGUNAS MECANICAS
		
		ticks = 3; // se usa para retardar algunos sucesos

		
		
		//#######################################################
		//			Arreglo de Obstaculos
		
		this.obstaculos = dameObstaList(cantiObstaculos);			//CREA UN ARREGLO DE OBSTACULOS
		
		//#######################################################
		//				VARIABLES Puntajes, vidas		 	   //
		//#######################################################
		enPausa = false;
		seGuardo = false;		// VARIABLE PARA QUE SOLO SE GUARDE 1 VEZ EN EL RANKING DE SER NECESARIO
		playerName = nombreJugador; // NOMBRE CON EL QUE SE GUARDARA EN EL RANKING
		
		score = 0;				// PUNTAJES ACTUALES

		vidasTotal = 3;	  // ASIGNA LA CANTIDAD DE VIDAS INICIALES
		vidasMaximas =5;  //### VARIABLE PARA EL MAXIMO DE VIDAS POSIBLES
		
		
		//########################################################
		
		// Inicia el juego!
		this.entorno.iniciar();
		
		
		
	}
	

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	//#######################################################
	//					INICIA METODO TICK				   //
	//#######################################################
	
	public void tick()
	{
		
		if(vidasTotal > 0 && !enPausa) {
			//#######################################################
			//			BLOQUE PARA LAS IMAGENS DE FONDO		   //
			//#######################################################
			this.entorno.dibujarImagen(imagenFondo, 400,290,0,0.5);

			
			this.arboles.dibujar(entorno);
			this.arboles.mover();
			if (this.arboles.getX() < -100) {
				this.arboles = this.dameArboles();
			}
			this.entorno.dibujarImagen(suelo, 400, 556, 0,0.2);
			
			
			//##########################################################################
			//	CONDICIONAL ENCARGADA DE LA REPRESENTACION DE LAS VIDAS EN PANTALLA   //
			//##########################################################################
			
			if(vidasTotal > 0) {
				int separacion = 0;
				for(int i = 0; i < vidasTotal ; i++) {
					this.entorno.dibujarImagen(Corazon, 760 - separacion, 50, 0, 0.1);
					separacion = separacion + 60;
				}
			}
			
			if((maxFire - unaBola) > 0) {
				int separacion = 0;
				for(int i = 0; i < (maxFire - unaBola) ; i++) {
					this.entorno.dibujarImagen(fueguillos, 40 + separacion, 100, 0, 0.8);
					separacion = separacion + 60;
				}
			}
	
			
			//#######################################################
			//					BLOQUE OBSTACULOS		  		   //
			//#######################################################
			
			for (int i = 0; i < obstaculos.length; i++) {
				
				this.obstaculos[i].mover();
				if(this.obstaculos[i].chocaObsta(this.Princesa.x1(), this.Princesa.x2(), this.Princesa.y2())) {
					siChoco = true;
					
				}
				this.obstaculos[i].dibujar(this.entorno);
				indice = i;	
			}
			
			if(this.obstaculos[indice].getX() <= -300) {
				this.obstaculos = dameObstaList(cantiObstaculos); // SE CREA UN NUEVO ARREGLO PARA GENERAR UN LOOP
				
			}
				
			
			//#######################################################
			//					BLOQUE BALAS DE CANON		  	   //
			//#######################################################
			if(hayBalas) {													//ESTA PARTE SE EJECUTA DEPENDIENDO EL NIVEL DE DIFICULTAD
				if(ticks % 2000 == 0) {
					this.balas = dameBalas();
				}
				if(ticks > 2200 ) {
					this.balas.mover();
					if(this.balas.chocaBala(this.Princesa.x1(), this.Princesa.x2(),this.Princesa.y1(), this.Princesa.y2())) { 	//VERIFICO SI HAY COLISION
						siChoco = true;
					}
					this.balas.dibujar(this.entorno);
				}
			}
			
			//#######################################################
			//			BLOQUE ITEMS  DE VIDA Y DEMAS		  		   //
			//#######################################################
			
			if(ticks % 3500 == 0) {		// LA CONDICIONAL ES UNA FORMA DE CREAR EL ITEM CADA CIERTO TIEMPO
				
				hayItem = true;
				this.vidas = dameCorazon();
			}
			if(!chocaVida() && hayItem) {
				this.vidas.mover();
				this.vidas.dibujar(this.entorno);
			}
			
			if(chocaVida() && hayItem) {						//SE CONTROLA SI SE OBTUVO UN ITEM , Y SE APLICA 1 VIDA EXTRA O PUNTOS
				if (vidasTotal < vidasMaximas) {
					Herramientas.play("1-up.wav");
					Herramientas.play("yoohoo.wav");
					vidasTotal +=1;
					hayItem = false;
				}
				if (vidasTotal == vidasMaximas) {
					Herramientas.play("coin.wav");
					score = score + 50;	
					hayItem = false;
				}
			}
			
	
			//#######################################################
			//					BLOQUE SOLDADO			  		   //
			//#######################################################
		
			for(int i = 0; i < soldados.length; i++) {
				if (soldados[i] != null) {
					this.soldados[i].mover();
					if(this.soldados[i].chocaSoldado(this.Princesa.x1(), this.Princesa.x2(), this.Princesa.y2())) {
						siChoco = true;
						//soldado = soldados[i];
						
					}
					soldados[i].dibujar(this.entorno);
					
				}
				
				if(unaBola > 0) {
					for (int j = 0; j < fireballs.length; j++) {
						if(this.fireballs[j] !=null && this.soldados[i] !=null) {
							if(this.soldados[i].FuegoySoldado(this.fireballs[j].x2(), this.fireballs[j].y2(), this.fireballs[j].y1())) {
								this.fireballs[j] = null;
								unaBola -=1;
								this.soldados[i] = null;
								Herramientas.play("burning.wav");
								score = score + 5;
							}
						}
					}
				}
				
			
				if(this.soldados[i] != null && this.soldados[i].getX() < 0) {
					this.soldados[i] = null;
				}
				
			}
			
			if(todosMuertos(soldados)) {			//CONTROLA SI TODOS LOS SOLDADOS MURIERON DE SER ASI CREA UNA NUEVA LISTA
				soldados = dameSoldaList(cantiSoldados);
				reseteaLista(soldadoSube);
				reseteaLista(soldadoBaja);
				
				
			}
		
			
			// CONTROL DEL SALTO DEL SOLDADO  CADA X TIEMPO
			for(int i = 0 ; i < soldados.length;i++) {
				if(soldados[i] !=null) {
					if (ticks %436 ==0 && soldados[i].getY() == damePiso()) {
						Herramientas.play("soldierJump.wav");
						soldadoSube[i] = true;
					}
					if(soldadoSube[i] && soldados[i].getY() > damePiso() - (soldados[i].getAlto() * 2) ) {
						soldados[i].subir();
					} else {
						soldadoSube[i] = false;
						soldadoBaja[i] = true;
						}
					
					if(soldadoBaja[i] && soldados[i].getY() < damePiso()){
						soldados[i].caer();
					}
					if (soldados[i].getY() == damePiso()) {
						soldadoBaja[i] = false;
					}
					
				}
			}
			
			
			
			
			
			
			
			// SUMA DE VARIABLE TICKS USADA COMO CONTROL DE TIEMPO
			ticks++;
			
			
			if (this.noMeVeas) {
				this.Princesa.dibujar(this.entorno);
			}
			
			
	
			
			//###############################################################
			//		BLOQUE DE CONTROL DE MOVIMIENTOS DE LA PRINCESA		   //
			//###############################################################
			
			
			//CONTROL DE TECLAS PRESIONADAS Y LAS ACCIONES DE LA PRINCESA
			
			
			if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && Princesa.getX() < ((this.entorno.ancho() /3)*2 - Princesa.getAncho()/2)){
				Princesa.moverDerecha();
			}
			
			if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && Princesa.getX() > (Princesa.getAncho()/2)){
				Princesa.moverIzquierda();
			}
			
			
			// CONTROL DE SALTO DE LA PRINCESA
			
			if (this.entorno.estaPresionada(this.entorno.TECLA_ARRIBA) && Princesa.getY() == damePiso()) {
				Herramientas.play("jump.wav");
				princesaSube = true;
				this.noMeVeas = false;
			}
			
			if(princesaSube && Princesa.getY() > damePiso() - (Princesa.getAlto() * 1.15 ) ) {	// CONFIGURA LA ALTURA MAXIMA DEL SALTO
				Princesa.subir();
				entorno.dibujarImagen(princesaSalto, Princesa.getX(), Princesa.getY(), 0);
			} else {
				
				princesaSube = false;
				princesaBaja = true;
				}
			
			if(princesaBaja && Princesa.getY() < damePiso()){
				Princesa.caer();
				entorno.dibujarImagen(princesaSalto, Princesa.getX(), Princesa.getY(), 0);
			}
			if (Princesa.getY() == damePiso()) {
				princesaBaja = false;
				this.noMeVeas = true;
			}
			
			
			//###################################################################
			//	CONTROL DE COLISION PRINCESA INMUNIDAD Y DISMINUCION DE VIDAS  //
			//###################################################################
			if(siChoco && !esInmune ) {	// AL SUFRIR UN DANIO SE OTORGA 3 SEG DE INMUNIDAD
				Herramientas.play("ooh.wav");
				siChoco = false;
				esInmune = true;
				time = 0;
				vidasTotal --;
			}
			
			if(esInmune && time < 3 ) {			//CONTROL DEL TIEMPO DE INMUNIDAD 
				siChoco = false;
				if(ticks% 100 == 0) {
					time = time + 1;
	
				}
			}else {
				esInmune = false;
			}
				
			
			
			
	
			
			//###############################################################
			//				BLOQUE DE CONTROL DE BOLA DE FUEGO	 		   //
			//###############################################################
			
			if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && unaBola < maxFire) {
				Herramientas.play("fireball.wav");
				unaBola = unaBola + 1;
				int disponible = 0;
				if(disponible < 1) {
					for (int i = 0; i < fireballs.length; i++) {
						if(fireballs[i] == null && disponible == 0) {
							fireballs[i] = this.Princesa.disparar(velocidad);
							disponible +=1;
						}
					}	
				}
				disponible = 0;
			}
			
			
			if(unaBola > 0) {
				for (int i = 0; i < fireballs.length; i++) {
					if(fireballs[i] !=null) {
						fireballs[i].mover();
						fireballs[i].dibujar(this.entorno);
						
						if(fireballs[i].getX() >= 800) {
							fireballs[i] = null;
							if(unaBola > 0) {
								unaBola -=1;
							}
							
						}
					}
				}
			}
			
			
			
			
			//###############################################################
			//					MUESTRA EL PUNTAJE ACTUAL			 	   //
			//###############################################################
			
			
			this.entorno.cambiarFont(Font.SANS_SERIF, 40, Color.WHITE);
			this.entorno.escribirTexto("Score  " + score, 10, 50);
			
			
			
			//###############################################################
			//							PAUSA EL JUEGO		 			   //
			//###############################################################
			if(this.entorno.sePresiono(this.entorno.TECLA_ENTER)) {
				Herramientas.play("soundPause.wav");
				enPausa = true;
				
			}
		
		
		} else if(vidasTotal > 0 && enPausa) {    //########### ESTO APARECE EN LA PAUSA
			
			this.entorno.dibujarImagen(pause, 400, 300, 0, 0.5);
			this.entorno.cambiarFont(Font.SANS_SERIF, 40, Color.WHITE);
			this.entorno.escribirTexto(" Presione Enter para continuar " , 115, 545);
			
			if(this.entorno.sePresiono(this.entorno.TECLA_ENTER)) {
				enPausa = false;
				Herramientas.play("soundPause.wav");
			}
		}
		else {
			//###############################################################
			//							GAME OVER					 	   //
			//###############################################################
			musiquita.stop();
			this.entorno.dibujarImagen(gameOver, 400, 300, 0);
			this.entorno.cambiarFont(Font.SANS_SERIF, 40, Color.WHITE);
			this.entorno.escribirTexto(" Presione Enter para continuar " , 115, 545);
			if(!seGuardo) {
				Herramientas.play("smb_gameover.wav");
				Jugada jugador = new Jugada(score,playerName);
				Ranking.armaRank(jugador);
				seGuardo = true;	
			}  
			
			//#########################################################################
			//		SI SE PRESIONA ENTER , CIERRA EL ENTORNO Y TE ENVIA MENU INICIAL
			if(this.entorno.sePresiono(this.entorno.TECLA_ENTER)) {
				MenuJuego.Principal.main(null);
				this.entorno.dispose();
			}
			
		}
		
	}
	

	

	@SuppressWarnings("unused")
	public static void main(String nombre,boolean dificultad,boolean musica){
		boolean nivel = dificultad;
		String player = nombre;
		boolean music = musica;
		new Juego(player,nivel,music);
	}
}
