package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	private Image princesa;
	private double x;
	private double y;
	private double ancho;
	private double alto;
	
	
	public Princesa(double x,double y, double ancho , double alto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.princesa = Herramientas.cargarImagen("princesa.gif");
	}
	
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(princesa, this.x,this.y, 0);
	}
	
	public void moverDerecha() {
		this.x = this.x +2;
	}
	
	public void moverIzquierda() {
		this.x = this.x -2;
	}
	
	public void subir() {
			this.y = this.y - 2.2;
	}
	
	public void caer() {
		this.y = this.y + 2.2;
		
	}

	public double getAlto() {
		return alto;
	}


	public double getAncho() {
		return ancho;
	}


	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}
	
	public double x1() {
		double x1 = x - ancho /2;
		return x1;
	}
	
	public double x2() {
		double x2 = x + ancho /2;
		return x2;
	}
	
	public double y1() {
		double y1 = y - alto /2;
		return y1;
	}
	
	public double y2() {
		double y2 = y + alto /2;
		return y2;
	}
	
	public Fireball disparar(double velocidad) {
		Fireball hadouken = new Fireball(this.x + 10, this.y,30,velocidad);
		return hadouken;
	}

}
