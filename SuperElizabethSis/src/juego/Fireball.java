package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Fireball {
	private Image fuego;
	private double x;
	private double y;
	private double diametro;
	private double velocidad;
	
	public Fireball(double x,double y, double diametro, double velocidad) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.fuego = Herramientas.cargarImagen("Fireball1.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(fuego, this.x, this.y, 0);
	}
	
	public void mover() {
		this.x += 2.7 + velocidad;
		
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	public double getDiametro() {
		return diametro;
	}
	public double y1() {
		double y1 = y - (diametro /2);
		return y1;
	}
	
	public double y2() {
		double y2 = y + (diametro /2);
		return y2;
	}
	public double x2() {
		double x2 = x + (diametro/2) ;
		return x2;
	}

	
	
}
