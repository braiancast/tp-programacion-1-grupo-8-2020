package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
	
	private Image planta;
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;

	
	public Obstaculo (double x,double y,double ancho,double alto,double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.planta = Herramientas.cargarImagen("obsta.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(planta, this.x, this.y, 0);
	}
	
	public void mover() {
		this.x -= 1.8 + velocidad;
		
	}
	
	//#######################################################
	//			METODOS DE OBTENCION DE COORDENADAS		   //
	//#######################################################

	public double getX() {
		return x;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	
	public double x1() {
		double x1 = x - ancho /2;
		return x1;
	}
	
	public double x2() {
		double x2 = x + ancho /2;
		return x2;
	}
	
	public double y1() {                 //Este hay que sacar lo dejo provisoriamente
		double y1 = y - alto /2;
		return y1;
	}

	public double getY() {
		return y;
	}
	
	//#######################################################
	//			METODO DE CONTROL DE COLISION		       //
	//#######################################################
	
	public boolean chocaObsta(double objetoX1 ,double objetoX2,double objetoY ) {
		
		if ( this.x1() <=  objetoX2 && this.x1() >=  objetoX1 && this.y1() <= objetoY){
			return true;
		}
		if(this.x2() >= objetoX1 && this.x2() <= objetoX2 && this.y1() <= objetoY){
			return true;
		}
		return false;
	}


}



