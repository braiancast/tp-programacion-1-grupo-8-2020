package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Bala {
	
	private Image bala;
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	
	public Bala (double x,double y,double ancho,double alto, double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		this.bala = Herramientas.cargarImagen("Bala11.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(bala, this.x, this.y, 0);
	}
	
	
	public void mover() {
		this.x -= 1.7 + velocidad;
		
	}

	public double getX() {
		return x;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	
	public double x1() {
		double x1 = x - ancho /2;
		return x1;
	}
	
	public double x2() {
		double x2 = x + ancho /2;
		return x2;
	}
	
	public double y1() {                 //Este hay que sacar lo dejo provisoriamente
		double y1 = y - alto /2;
		return y1;
	}

	public double getY() {
		return y;
	}
	
	//#######################################################
	//			METODO DE CONTROL DE COLISION		       //
	//#######################################################
		
	public boolean chocaBala(double objetoX1 , double objetoX2 ,double objetoY1 , double objetoY2 ) {
		
		if ( this.x1() <=  objetoX2 && this.x1() >=  objetoX1 && this.getY() <= objetoY2 && this.getY() >= objetoY1 ){
			return true;
		}
		if(this.x2() >= objetoX1 && this.x2() <= objetoX2 && this.getY() <= objetoY2 && this.getY() >= objetoY1 ) {
			return true;
		}
		return false;
	}

}
