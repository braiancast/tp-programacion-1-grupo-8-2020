package juego;

import java.awt.Image;

import entorno.Entorno;

public class MovimientoFondo {
	private Image imagen;
	private double x;
	private double y;
	private double velocidad;

	
	public MovimientoFondo ( Image imagen,double x,double y, double velocidad) {
	
	this.x = x;
	this.y = y;
	this.velocidad = velocidad;
	this.imagen = imagen;
	}
	
	public void dibujar(Entorno e) {
		
		e.dibujarImagen(imagen, this.x, this.y, 0,1.8);
	}
		
	public void mover() {
		this.x -= 1.7 + velocidad;
	}
	
	
	public double getX() {
		return x;
	}
	
}

