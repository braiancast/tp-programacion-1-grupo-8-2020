package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Vida {
	private Image 	corazon;
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	
	
	public Vida (double x,double y,double ancho,double alto, double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		corazon = Herramientas.cargarImagen("1up.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(corazon, this.x, this.y, 0);
	}
	
	public void mover() {
		this.x -= 1 + velocidad;
		
	}
	
	//#######################################################
		//			METODOS DE OBTENCION DE COORDENADAS		   //
		//#######################################################

		public double getX() {
			return x;
		}

		public double getAncho() {
			return ancho;
		}

		public double getAlto() {
			return alto;
		}
		
		public double x1() {
			double x1 = x - ancho /2;
			return x1;
		}
		
		public double x2() {
			double x2 = x + ancho /2;
			return x2;
		}
		
		public double y1() {                 //Este hay que sacar lo dejo provisoriamente
			double y1 = y - alto /2;
			return y1;
		}

		public double getY() {
			return y;
		}
		

}
