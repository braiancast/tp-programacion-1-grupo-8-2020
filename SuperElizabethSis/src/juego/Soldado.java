package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {
	private Image  soldier76;
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	
	public Soldado (double x,double y,double ancho,double alto,double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		this.soldier76 = Herramientas.cargarImagen("soldado76.gif");
	}
	
	public void dibujar(Entorno e) {
		
		e.dibujarImagen(soldier76, this.x, this.y, 0);
	}
	
	public void mover() {
		this.x -= (0.9 + velocidad);
		
	}
	
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	public void subir() {
		this.y = this.y - 2;
	}
	
	public void caer() {
		this.y = this.y + 2;
		
	}

	
	public double x1() {
		double x1 = x - ancho /2;
		return x1;
	}
	
	public double x2() {
		double x2 = x + ancho /2;
		return x2;
	}
	
	public double y1() {
		double y1 = y - alto /2;
		return y1;
	}
	
	public double y2() {
		double y2 = y + alto /2;
		return y2;
	}
	
	//#######################################################
	//			METODO DE CONTROL DE COLISION		       //
	//#######################################################
		
	public boolean chocaSoldado(double objetoX1 ,double objetoX2,double objetoY ) {
		if ( this.x1() <=  objetoX2 && this.x1() >=  objetoX1 && this.y1() <= objetoY){
			return true;
		}
		if(this.x2() >= objetoX1 && this.x2() <= objetoX2 && this.y1() <= objetoY){
			return true;
		}
		return false;
	}
	
	
	public boolean FuegoySoldado(double fireX2 ,double fireY2 , double fireY1 ) {
		
		if( fireX2 >= this.x1() && fireX2 <= this.x2() && fireY2 >= this.y1() && fireY1 <= this.y2()) {
			return true;
		}
		return false;
	}

}
