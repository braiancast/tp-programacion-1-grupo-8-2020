package MenuJuego;

import javax.sound.sampled.Clip;

import entorno.Herramientas;

public class Principal {
	
	
	static Clip audio = Herramientas.cargarSonido("prelude.wav");
	
	
	public static void main(String[] args) {
		Arranca intro1 = new Arranca();
		intro1.setVisible(true);
		
		
	}
	
	
	
	public static void tocarIntro() {
		 audio.start();
	}
	
	
	public static void pararIntro() {
		 audio.stop();
	}
	
	public static void cerrarIntro() {
		 audio.close();
	}
	
	public static void tocarEnLoop() {
		audio.loop(Clip.LOOP_CONTINUOUSLY);
	}
	

}
