package MenuJuego;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import TablaRank.Ranking;

import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Toolkit;

public class VentaRanking extends JFrame {

	private JPanel contentPane;


	
	public VentaRanking() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentaRanking.class.getResource("/trofeo.png")));
		setBackground(Color.BLACK);
		setTitle("RANKING");
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 450);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ventana1 menu1 = new Ventana1();
				menu1.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 15));
		btnNewButton.setBounds(160, 356, 147, 38);
		contentPane.add(btnNewButton);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(Color.WHITE);
		textArea.setForeground(new Color(0, 0, 0));
		textArea.setEditable(false);
		textArea.setFont(new Font("MS PGothic", Font.BOLD | Font.ITALIC, 20));
		textArea.setBounds(10, 49, 474, 270);
		textArea.append(Ranking.pasaAString(Ranking.armaTabla()));
		contentPane.add(textArea);
		
		JLabel lblNewLabel = new JLabel("RANK\t\t\t \t\t          \t\t  SCORE\t            \t\t  \t\t\t\t\t   PLAYER");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel.setBounds(48, 11, 380, 27);
		contentPane.add(lblNewLabel);
	}
}
