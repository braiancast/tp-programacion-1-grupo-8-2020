package MenuJuego;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class VentaOpcion extends JFrame {

	private JPanel contentPane;
	private JTextField player_Name;
	private final ButtonGroup btnsDificultades = new ButtonGroup();
	JRadioButton radio1,radio2,radioON,radioOFF;
	JButton botonPlay,botonVolver;
	private final ButtonGroup btnsMusica = new ButtonGroup();
	

	
	
	
	public VentaOpcion() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				Principal.pararIntro();
			}
		});
		setResizable(false);
		setTitle("Opciones");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentaOpcion.class.getResource("/catito.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		iniciarComponentes();
	}	
	
	private void iniciarComponentes() {
		
		//#########################################################
		// 			ETIQUETAS
		//#########################################################
		JLabel lblNewLabel = new JLabel("Nombre Jugador");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBackground(Color.BLACK);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel.setBounds(10, 11, 161, 28);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Dificultad");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewLabel_1.setBounds(10, 59, 106, 33);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblmusica = new JLabel("Musica");
		lblmusica.setForeground(Color.RED);
		lblmusica.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblmusica.setBounds(10, 103, 106, 33);
		contentPane.add(lblmusica);
		
		//#########################################################
		// 			CAMPO PARA RELLENA EL NOMBRE DEL JUGADOR
		//#########################################################
		
		player_Name = new JTextField();
		player_Name.setBounds(195, 17, 161, 20);
		contentPane.add(player_Name);
		player_Name.setColumns(10);
		
		
		
		
		//#########################################################
		// 						BOTONES
		//#########################################################
		
		radio1 = new JRadioButton("Normal",true);
		btnsDificultades.add(radio1);
		radio1.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		radio1.setBounds(148, 59, 109, 23);
		contentPane.add(radio1);
		
		radio2 = new JRadioButton("Dificil");
		btnsDificultades.add(radio2);
		radio2.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		radio2.setBounds(278, 59, 109, 23);
		contentPane.add(radio2);
		
		
		radioON = new JRadioButton("ON",true);
		btnsMusica.add(radioON);
		radioON.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		radioON.setBounds(148, 111, 109, 23);
		contentPane.add(radioON);
		
		radioOFF = new JRadioButton("OFF");
		btnsMusica.add(radioOFF);
		radioOFF.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		radioOFF.setBounds(278, 111, 109, 23);
		contentPane.add(radioOFF);
		
		//#########################################################
		// 		BOTON PLAY PARA DAR COMIENZO AL JUEGO
		//#########################################################
		
		botonPlay = new JButton("Play");
		botonPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Principal.pararIntro();
				String jugador = nameJugador();
				boolean esFacil = validaRadios();
				boolean hayMusica = validaMusica();
				
				
				juego.Juego.main(jugador,esFacil,hayMusica);
				dispose();
			}
		});
		
		botonPlay.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonPlay.setBounds(148, 206, 89, 23);
		contentPane.add(botonPlay);
		
		
		//#########################################################
		// 						BOTON VOLVER
		//#########################################################
		
		botonVolver = new JButton("Volver");
		botonVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ventana1 menu1 = new Ventana1();
				menu1.setVisible(true);
				dispose();
			}
		});
		botonVolver.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonVolver.setBounds(335, 227, 89, 23);
		contentPane.add(botonVolver);
		
		//#########################################################
		// 					IMAGEN DE FONDO
		//#########################################################
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(VentaOpcion.class.getResource("/fondoMenu.gif")));
		lblNewLabel_2.setBounds(0, 0, 434, 261);
		contentPane.add(lblNewLabel_2);
		
		
		
		
	}
	
	//#########################################################
	// 					IMAGEN DE FONDO
	//#########################################################
	public String nameJugador() {
		String jugador;
		jugador = " " + player_Name.getText();
		
		return jugador;
	}
	
	public boolean validaRadios() {
		if (radio1.isSelected()) {
			return true;
		}
		if (radio2.isSelected()) {
			return false;
		}
		return true;
	}
	
	public boolean validaMusica() {
		if (radioON.isSelected()) {
			return true;
		}
		if (radioOFF.isSelected()) {
			return false;
		}
		return true;
	}
}
