package MenuJuego;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import entorno.Herramientas;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Arranca extends JFrame {

	private JPanel contentPane;


	public Arranca() {
		Principal.tocarEnLoop();
		setTitle("\t\t\t\t\t\tIntro");
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				Ventana1 menu1 = new Ventana1();
				menu1.setVisible(true);
				dispose();
			}
		});
		setForeground(Color.WHITE);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 606, 484);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		//Herramientas.play("prelude.wav");
		
		
		JLabel showVideo = new JLabel("");
		showVideo.setIcon(new ImageIcon(Arranca.class.getResource("/introgif.gif")));
		showVideo.setBounds(0, 0, 600, 391);
		contentPane.add(showVideo);
		
		JLabel pressAnyKey = new JLabel("");
		pressAnyKey.setIcon(new ImageIcon(Arranca.class.getResource("/pressAnykey.gif")));
		pressAnyKey.setBounds(78, 402, 440, 42);
		contentPane.add(pressAnyKey);
		
	
		
		
	}
}
