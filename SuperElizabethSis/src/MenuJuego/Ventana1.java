package MenuJuego;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class Ventana1 extends JFrame {

	private JPanel contentPane;
	
	final VentaRanking frame = new VentaRanking();
	final VentaOpcion opcionFrame = new VentaOpcion();
	final String instrucciones;
	
	
	
	
	
	public Ventana1() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana1.class.getResource("/catito.png")));
		setForeground(Color.BLACK);
		setResizable(false);
		setTitle("Launcher");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 450);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//Principal.tocarEnLoop();
		//########################################################################
		//		MENSAJE DE INSTRUCCION
		instrucciones="Instrucciones\r\n" + 
				"\r\n" + 
				"Si se presiona la fecha hacia atras, la princesa retrocedera.\r\n" + 
				"\r\n" + 
				"Si se presiona la fecha hacia adelante, la princesa avanzara.\r\n" + 
				"\r\n" + 
				"Si se presiona la fecha hacia arriba, la princesa saltara. \r\n" + 
				"\r\n" + 
				"Si se presiona la barra espaciadora, la princesa lanzara una pequena bola de fuego.\r\n" + 
				"\r\n" + 
				"Si la princesa toca un obstaculo o un soldado pierde una vida.\r\n" + 
				"\r\n" + 
				"Si la princesa pierde las tres vidas, perdemos y se termina el juego.\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Por cada soldado que la princesa elimine se incrementa en 5 su puntaje";
		//########################################################################
		//		BOTON JUGAR , INICIA LA CLASE JUEGO
		
		JButton botonJugar = new JButton("Jugar");
		botonJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				opcionFrame.setVisible(true);
				dispose();
			}
		});
		botonJugar.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonJugar.setBounds(10, 200, 220, 40);
		contentPane.add(botonJugar);
		
		JButton botonTablade = new JButton("Tabla de Posiciones");
		botonTablade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(true);
				
				dispose();
			}
		});
		botonTablade.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonTablade.setBounds(10, 254, 220, 40);
		contentPane.add(botonTablade);
		
		
		//########################################################################
		//		BOTON COMO JUGAR ABRE UN PANEL  DONDE DA LAS INSTRUCCIONES
		
		JButton botonComoJugar = new JButton("Como jugar");
		botonComoJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, instrucciones, "Instrucciones", JOptionPane.INFORMATION_MESSAGE);
				
			}
		});
		botonComoJugar.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonComoJugar.setBounds(10, 305, 220, 40);
		contentPane.add(botonComoJugar);
		
		
		//########################################################################
		//		BOTON COMO ACERCA DE  , MUESTRA QUIEN PROGRAMO EL JUEGO
				
		JButton botonAcercaDe = new JButton("Acerca de ...");
		botonAcercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Trabajo practico final\n" + "Realizado por el Grupo 8\n "+ "\n \n Video realizado por Sharon C");
			}
		});
		botonAcercaDe.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonAcercaDe.setBounds(10, 356, 220, 40);
		contentPane.add(botonAcercaDe);
				
		
		//########################################################################
		//		BOTON SALIR  , CIERRA LA VENTANA Y EL JUEGO.
		
		JButton botonSalir = new JButton("Salir");
		botonSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		botonSalir.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 14));
		botonSalir.setBounds(306, 361, 151, 31);
		contentPane.add(botonSalir);
		
		
		//########################################################################
		//		SE COLOCO  UNA IMAGEN DE FONDO
		
		
		JLabel show_image = new JLabel("");
		show_image.setBackground(Color.WHITE);
		show_image.setIcon(new ImageIcon(Ventana1.class.getResource("/fondoMenu.gif")));
		show_image.setBounds(0, 0, 474, 399);
		contentPane.add(show_image);
		

	}
}
