package TablaRank;

public class Jugada {
	
	private	int puntos;
	private	String nombre;
	
	public Jugada(int puntaje, String nombre) {
		this.puntos = puntaje;
		this.nombre	= nombre;
	}
	
	
	static boolean entraRank(Jugada [] listin,Jugada actual) {
		int ultimo = listin.length - 1;
		if(actual.puntos > listin[ultimo].puntos) {
			listin[ultimo] = actual;
			return true;
		}
		return false;
	}
	
	static void ordenaBurbuja(Jugada arreglo[]){
		
        for(int i = 0; i < arreglo.length - 1; i++)
        {
            for(int j = 0; j < arreglo.length - 1; j++)
            {
                if (arreglo[j].puntos < arreglo[j + 1].puntos)
                {
                    Jugada tmp = arreglo[j+1];
                    arreglo[j+1] = arreglo[j];
                    arreglo[j] = tmp;
                }
            }
        }
    }


	public int getPuntos() {
		return puntos;
	}


	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
