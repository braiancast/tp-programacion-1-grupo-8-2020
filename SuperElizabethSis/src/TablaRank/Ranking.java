package TablaRank;

public class Ranking {
	
	
	//###################################################################################
	//	CREA SI NO EXISTE EL RANK, LO LEE COMPARA PUNTAJES , LO EDITA LO ACOMODA Y GUARDA
	
	public static void armaRank(Jugada candidato) {	
		Jugada [] mejores;
		
		mejores = new Jugada[10];
		
		CargaEditaTexto.creaRanking(mejores);
		
		CargaEditaTexto.leeArchivo(mejores);
		
		Jugada.entraRank(mejores, candidato);		
	
		
		Jugada.ordenaBurbuja(mejores);
		
		
		CargaEditaTexto.guardaRanking(mejores);
	}
	
	//#######################################################################################
	//		ARMA UNA TABLA EN CASO DE NO EXISTIR Y DE VUELVE UN ARREGLO SE USA PARA EL MENU
	public static Jugada[] armaTabla() {
		Jugada [] mejores;
		
		mejores = new Jugada[10];
		
		CargaEditaTexto.creaRanking(mejores);
		
		CargaEditaTexto.leeArchivo(mejores);
		
		Jugada.ordenaBurbuja(mejores);
		
		CargaEditaTexto.guardaRanking(mejores);
		
		return mejores;
	}
	
	//#########################################################################
	//					LEE UN ARREGLO DEL OBJETO JUGADA Y LO CONVIERTE EN STRING
	public static String pasaAString(Jugada [] liston) {
		String mensaje = "";
		for (int i = 0; i < liston.length; i++) {
			mensaje = mensaje +  "      "+(i+1)+ "	      " + liston[i].getPuntos() + "	      " + liston[i].getNombre() + "\n";
		}
		return mensaje;
	}
	

}
