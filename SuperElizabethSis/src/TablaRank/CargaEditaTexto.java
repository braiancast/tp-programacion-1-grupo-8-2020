package TablaRank;
import java.io.*;


public class CargaEditaTexto {
	

	
	public static void leeArchivo(Jugada [] lista) {
		
		FileReader	lectorArchivo = null;
		try {
			lectorArchivo	= new FileReader("ranking.txt");
		} catch (FileNotFoundException e) {
			
		}
		
		BufferedReader textoArchivo;
		textoArchivo = new BufferedReader(lectorArchivo);
		
		int casilla;
		casilla = 0;
		String	lineaTexto;
		
		while(casilla < lista.length ) {
			
			try {
				lineaTexto = textoArchivo.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
			String[] valores;
			valores = lineaTexto.split(";");
			int punto;
			punto = Integer.parseInt(valores[0]);
			
			lista[casilla] = new Jugada(punto,valores[1]);
			casilla++;
		}
		
		
		try {
			textoArchivo.close();		// SE CIERRA EL TEXTO
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	
	
	static void guardaRanking(Jugada [] ListaOrde) {
		File f;						// PREPARO EL ARCHIVO DECLARANDO LAS VARIABLES
		FileWriter w;
		BufferedWriter bw;
		PrintWriter wr;
		String nombre = "ranking.txt";	//Nombre Archivo ya lo dejo configurado
		
		try {
			
			f = new File(nombre);		
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
			wr = new PrintWriter (bw);
			
			for(int i =0; i < ListaOrde.length; i++) {
				wr.write(ListaOrde[i].getPuntos()+ ";" + ListaOrde[i].getNombre() + "\n");
				//wr.append("supongo que la sig");
			}
			
			wr.close();			//CIERRO
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	static void creaRank00(Jugada [] ListaOrde) {
		File f;						// PREPARO EL ARCHIVO DECLARANDO LAS VARIABLES
		FileWriter w;
		BufferedWriter bw;
		PrintWriter wr;
		String nombre = "ranking.txt";	//Nombre Archivo ya lo dejo configurado
		
		try {
			
			f = new File(nombre);		
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
			wr = new PrintWriter (bw);
			
			for(int i =0; i < ListaOrde.length; i++) {
				wr.write(0 + ";" + 0+ "\n");
				//wr.append("supongo que la sig");
			}
			
			wr.close();			//CIERRO
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	static void creaRanking(Jugada [] rnkList) {
		File archivo = new File("ranking.txt");
		if(!archivo.exists()) {
			creaRank00(rnkList);
		}
	}

}
